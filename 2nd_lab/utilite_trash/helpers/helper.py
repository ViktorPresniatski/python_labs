import os


def create_path_if_not_exists(*args):
    for path in args:
        if not os.path.exists(path):
            os.makedirs(path)


def request(question, force=False):
    if force:
        return True
    positive = ['y', 'yes']
    answer = raw_input(question) # !!!
    if answer.lower() in positive:
        return True
    else:
        return False


def formated_size(size):
    size = int(size)
    kb = 1024
    mb = 1024 ** 2
    gb = 1024 ** 3
    if size < kb:
        return str(size) + "b"
    elif size < mb:
        return str(size / kb) + "kb"
    elif size < gb:
        return str(size / mb) + "mb"
    else:
        return str(size / gb) + "gb"


def formated_string(name, ftype, path, deletion_date, size):
    return "{0:15} {1:10} {2:40} {3:23} {4:5}\n"\
        .format(name, ftype, path, deletion_date, size)
