import argparse


def parse_args():
    """Parse arguments from console"""

    parser = argparse.ArgumentParser("trash", description="Program "
                                     "simulating the trash")
    parser.add_argument("--config-file", type=str, default=None,
                        help="set config file")
    parser.add_argument("--trash", type=str, default=None,
                        help="set trash path")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="explain what is being done")
    parser.add_argument("-s", "--silent", action="store_true",
                        help="silent mode")
    parser.add_argument("--dry-run", action="store_true",
                        help="execute imitation of command")

    subparsers = parser.add_subparsers()
    parser_rm = subparsers.add_parser("rm",
                                      help="remove files or directories")
    parser_rm.add_argument("file", type=str,
                           help="remove files", nargs="+")
    parser_rm.add_argument("-f", "--force", action="store_true",
                           help="ignore nonexistent files and arguments, "
                           "never prompt")
    parser_rm.add_argument("-i", "--interactive", action="store_true",
                           help="prompt before every removal")
    parser_rm.add_argument("-r", "--recursive", action="store_true",
                           help="remove directories and their contents "
                           "recursively")
    parser_rm.add_argument("-d", "--dir", action="store_true",
                           help="remove empty directories")
    parser_rm.add_argument("--regex", type=str, default=None,
                           help="remove files or directories by pattern")
    parser_rm.set_defaults(operation="remove")

    parser_restore = subparsers.add_parser("restore", help="restore files"
                                           " or directories from trash")
    parser_restore.add_argument("file", type=str, nargs="*",
                                help="restore files or dirs")
    parser_restore.add_argument("--all", action="store_true",
                                help="restore all items from trash")
    parser_restore.set_defaults(operation="restore")

    parser_show = subparsers.add_parser("show", help="show items of trash")
    parser_show.set_defaults(operation="show")

    parser_clear = subparsers.add_parser("clear", help="clear trash")
    parser_clear.add_argument("file", type=str, nargs="*",
                              help="clear files or dirs from trash")
    parser_clear.add_argument("--all", action="store_true",
                              help="clear all items from trash")
    parser_clear.set_defaults(operation="clear")

    args = parser.parse_args()
    return args
