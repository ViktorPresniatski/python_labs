import os
import shutil


def F(path):
    path = os.path.abspath(path)
    if not os.path.lexists(path):
        raise OSError("File not found: {}".format(path))

    if os.path.islink(path):
        item = Link(path)
    elif os.path.isfile(path):
        item = File(path)
    elif os.path.isdir(path):
        item = Directory(path)
    else:
        raise OSError("Unknown items type")
    return item


class FileModel(object):

    def __init__(self, path):
        super(FileModel, self).__init__()
        self.original_path = os.path.abspath(path)
        self.name = os.path.basename(os.path.normpath(path))

    @property
    def description(self):
        if not os.access(self.original_path, os.W_OK):
            return "write-protected {0}".format(self.type)
        else:
            return self.type

    def __str__(self):
        return "{0}, path: {1}".\
            format(self.__class__.__name__, self.original_path)


class File(FileModel):

    def __init__(self, path):
        super(File, self).__init__(path)
        self.type = "file"
        self._size = os.path.getsize(self.original_path)

    def move(self, trash_path):
        os.renames(self.original_path, trash_path)

    def remove(self):
        os.remove(self.original_path)
        return True

    @property
    def size(self):
        return self._size


class Directory(FileModel):

    def __init__(self, path):
        super(Directory, self).__init__(path)
        self.type = "directory"
        self._size = self.get_size()

    def move(self, trash_path):
        subpaths = os.listdir(self.original_path)
        if len(subpaths) == 0:
            os.renames(self.original_path, trash_path)
        else:
            raise Exception("Directory is not empty")

    def remove(self):
        shutil.rmtree(self.original_path)
        return True

    def listdir(self):
        def func(path):
            return F(os.path.join(self.original_path, path))
        subpaths = os.listdir(self.original_path)
        return map(func, subpaths)

    def get_size(self):
        def get_local_size(size, dr, file_list):
            for file_name in file_list:
                full_path = os.path.join(dr, file_name)
                if os.path.islink(full_path):
                    continue
                if os.path.isfile(full_path):
                    size[0] += os.path.getsize(full_path)
        size = [0]
        os.path.walk(self.original_path, get_local_size, size)
        return size[0]

    @property
    def size(self):
        return self._size


class Link(File):

    def __init__(self, path):
        super(Link, self).__init__(path)
        self.type = "link"


class Page(object):

    def __init__(self, name, type, path, deletion_date, size):
        self.col_name = name
        self.col_type = type
        self.col_path = path
        self.col_deletion_date = deletion_date
        self.col_size = size
        self.content = list()

    def add_row(self, name, type, path, deletion_date, size):
        item = PageItem(name, type, path, deletion_date, size)
        self.content.append(item)


class PageItem(object):

    def __init__(self, name, type, path, deletion_date, size):
        self.name = name
        self.type = type
        self.path = path
        self.deletion_date = deletion_date
        self.size = size
