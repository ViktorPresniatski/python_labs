import os
import re
from datetime import datetime

from utilite_trash.helpers.helper import request


class Policy(object):

    def remove(self, file, trash_path):
        if not os.access(file.original_path, os.W_OK):
            prompt = "rm: remove write-protected {0} \'{1}\'? ".\
                format(file.type, file.original_path)
            status = self.confirm_action(prompt, file.move, trash_path)
        else:
            file.move(trash_path)
            status = True
        return status

    def go_down_to_the_dir(self, dir, iteration_func):
        if not os.access(dir.original_path, os.W_OK):
            return AskPolicy().go_down_to_the_dir(dir, iteration_func)
        else:
            return iteration_func()

    def confirm_action(self, prompt, func, *args):
        response = False
        if request(prompt):
            func(*args)
            response = True
        return response


class AskPolicy(Policy):

    def remove(self, file, trash_path):
        prompt = "rm: remove {0} \'{1}\'? ".\
            format(file.description, file.original_path)
        if request(prompt):
            file.move(trash_path)
            return True
        else:
            return False

    def go_down_to_the_dir(self, dir, iteration_func):
        prompt = "rm: go down to the {0} \'{1}\'? ".\
            format(dir.description, dir.original_path)
        if request(prompt):
            return iteration_func()
        else:
            return 0

    def get_destination(self, path):
        prompt = "File name already exists in folder\n" + \
                 "Replace file? "
        if request(prompt):
            return path
        else:
            raise Exception("this name is already taken")

    def restore(self, file_path, dest_path):
        status = False
        prompt = "restore: path, inclusive this file, is not exists. Create? "
        dirname = os.path.dirname(dest_path)
        if not os.path.exists(dirname):
            status = self.confirm_action(prompt, os.renames,
                                         file_path, dest_path)
        return status


class ForcePolicy(Policy):

    def remove(self, file, trash_path):
        file.move(trash_path)
        return True

    def go_down_to_the_dir(self, dir, iteration_func):
        return iteration_func()


class DefaultPolicy(Policy):

    def get_destination(self, path):
        count = 1
        while os.path.exists(path):
            count += 1
            file_name, ext = os.path.splitext(path)
            if count == 2:
                new_file_name = "{0}_{1}".format(file_name, count)
            else:
                new_file_name = re.sub(r"_\d$", "_{0}".format(count),
                                       new_file_name)
            path = new_file_name + ext
        return path

    def restore(self, file_path, name_path):
        os.renames(file_path, name_path)
        return True


class ClearByDatePolicy(Policy):

    def __init__(self, timedelta):
        self.timedelta = timedelta
        super(ClearByDatePolicy, self).__init__()

    def get_trash(self, files):
        result = set()
        now = datetime.now()
        for file in files:
            file_name = file[0]
            info = file[1]["trash_info"]
            deletion_date = datetime.strptime(info["deletion_date"],
                                              "%Y-%m-%d %H:%M:%S")
            if now - deletion_date > self.timedelta:
                result.add(file_name)
        return result


class ClearBySizePolicy(Policy):

    def __init__(self, max_size):
        self.max_size = max_size
        super(ClearBySizePolicy, self).__init__()

    def get_trash(self, files):
        result = set()
        for file in files:
            file_name = file[0]
            size = file[1]["trash_info"]["size"]
            if size > self.max_size:
                result.add(file_name)
        return result


class DryRun(object):

    def run(self, func):
        def wrapped_func(*args):
            if self.dried:
                return True
            else:
                return func(*args)
        return wrapped_func
