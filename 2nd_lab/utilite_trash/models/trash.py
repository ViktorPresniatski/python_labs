import datetime
import json
import logging
import os
import re

from utilite_trash.helpers.helper import create_path_if_not_exists
from utilite_trash.helpers.helper import formated_size
from utilite_trash.models.file_model import F, Page
import utilite_trash.models.policy as policy_module


class Trash(policy_module.DryRun, object):

    def __init__(self, path="~/.trash/Trash", max_size=None,
                 name_duplicate="get_uniq", clear_policies=[]):
        self.logger = logging.getLogger("action.trash")
        self.dried = False
        self.path = os.path.expanduser(path)
        self.files_path = os.path.join(self.path, "files")
        self.info_path = os.path.join(self.path, "info")
        self.max_size = max_size
        self.clear_policies = clear_policies
        self._select_names_collision_policy(name_duplicate)

    def is_enough_space_for(self, file):
        if not self.max_size:
            return True
        free_trash_space = self.max_size - F(self.files_path).size
        return file.size < free_trash_space

    # def grep(self, dir_path, pattern):
    #     def walk(arg, dir, files):
    #         for file_name in files:
    #             if re.search(pattern, file_name):
    #                 file_path = os.path.join(dir, file_name)
    #                 file_list.append(file_path)
    #     self.logger.debug("find items by regex %s", pattern)
    #     file_list = []
    #     os.path.walk(dir_path, walk, file_list)
    #     return file_list
    def grep(self, root_directory, regexp):
        result = []
        root_directory = os.path.expanduser(root_directory)
        if not os.path.isdir(root_directory):
            return result
        try:
            sub_dirs = os.listdir(root_directory)
        except Exception as e:
            self.logger.error("ERROR: %s", e.message)
            sub_dirs = []
        queue = [os.path.join(root_directory, item) for item in sub_dirs]
        while queue:
            current = queue.pop()
            if re.search(regexp, os.path.basename(current)):
                result.append(current)
            elif os.path.isdir(current):
                try:
                    sub_dirs = os.listdir(current)
                except Exception as e:
                    self.logger.error("ERROR: %s", e.message)
                    sub_dirs = []
                queue.extend([os.path.join(current, item) for item in sub_dirs])
        return result

    def add(self, file, policy, remove_method="remove_recursive"):
        self.logger.debug("attempt to move to trash: \'%s\'",
                          file.original_path)
        create_path_if_not_exists(self.files_path)
        if not self.is_enough_space_for(file):
            raise Exception("not enough space in the trash, please clear it")
        status = False
        remove_method = getattr(self, remove_method)
        trash_path = remove_method(file, policy)
        if trash_path:
            self.run(self._set_trashinfo)(file, trash_path)
            self.logger.info("successfuly moved to trash: %s",
                             file.original_path)
            status = True
        else:
            self.logger.info("failure of moving to trash: %s",
                             file.original_path)

        return status

    def restore(self, name, restore_policy=policy_module.DefaultPolicy(),
                names_collision_policy=policy_module.DefaultPolicy()):
        self.logger.debug("attempt to restore file \'%s\' from trash", name)
        file_path, info = self._get_file_and_info(name)
        info_name = name + ".json"
        dest_path = self._get_destination(info["trash_info"]["path"],
                                          names_collision_policy)
        status = self.run(restore_policy.restore)(file_path, dest_path)
        if status:
            info_path = os.path.join(self.info_path, info_name)
            self.run(os.remove)(info_path)
            self.logger.info("restored \'%s\'", name)
        else:
            self.logger.info("not restored \'%s\'", name)
        return status

    def _get_trashinfo(self, name):
        info_path = os.path.join(self.info_path, name)
        info = {}
        if not os.path.exists(info_path):
            raise Exception("info file for \'{0}\' doesn't exist".format(name))
        with open(info_path) as file:
            try:
                info = json.load(file)
            except Exception:
                raise Exception("incorrect info file for \'{0}\'".format(name))
        return info

    def _set_trashinfo(self, file, new_path):
        create_path_if_not_exists(self.info_path)
        deletion_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        name = os.path.basename(new_path) + ".json"
        content = {"trash_info": {"path": file.original_path,
                                  "deletion_date": deletion_date,
                                  "size": file.size,
                                  "type": file.type}}
        with open(os.path.join(self.info_path, name), 'w') as f:
            json.dump(content, f, indent=4, separators=(',', ': '))
            self.logger.debug("trash info for \'%s\' was set", name)

    def get_files(self):
        return os.listdir(self.files_path)

    def _select_names_collision_policy(self, name_duplicate):
        if name_duplicate == "replace":
            self.names_collision_policy = policy_module.AskPolicy()
        else:
            self.names_collision_policy = policy_module.DefaultPolicy()

    def remove_recursive(self, file, policy, relative_name=""):
        relative_name = os.path.join(relative_name, file.name)
        self.logger.debug("recursion: %s", relative_name)
        if file.type == "file":
            return self.remove(file, policy, relative_name)
        try:
            subfiles = file.listdir()
        except Exception as e:
            self.logger.error("rm: %s", str(e))
            return None
        if len(subfiles) == 0:
            return self.remove(file, policy, relative_name)
        iteration_func = self._recursive_iteration(subfiles, policy,
                                                   relative_name)
        processed = policy.go_down_to_the_dir(file, iteration_func)
        if processed > 0:
            return os.path.join(self.files_path, file.name)
        else:
            return None

    def _recursive_iteration(self, subfiles, policy, relative_name):
        def iteration():
            processed = 0
            for f in subfiles:
                status = self.remove_recursive(f, policy, relative_name)
                if status:
                    processed += 1
            return processed
        return iteration

    def remove_empty_dir(self, file, policy):
        if not file.type == "directory":
            raise Exception("it is not directory")
        return self.remove(file, policy)

    def remove_file(self, file, policy):
        if file.type == "directory":
            raise Exception("it is directory")
        return self.remove(file, policy)

    def remove(self, file, policy, relative_name=None):
        self.logger.debug("attempt to execute removing %s \'%s\'",
                          file.description, file.original_path)
        file_name = relative_name if relative_name else file.name
        try:
            trash_path = os.path.join(self.files_path, file_name)
            trash_path = self._get_destination(trash_path,
                                               self.names_collision_policy)
            status = self.run(policy.remove)(file, trash_path)
            if status:
                self.logger.info("removed %s \'%s\'", file.type,
                                 file.original_path)
            return trash_path if status else None
        except Exception as e:
            self.logger.error("rm: cannot remove %s \'%s\': %s",
                              file.type, file.original_path, str(e))
            import traceback, sys; traceback.print_exc(file=sys.stdout)
            return None

    def _get_destination(self, new_path, policy):
        if os.path.exists(new_path):
            return policy.get_destination(new_path)
        return new_path

    def _get_file_and_info(self, name):
        file_path = os.path.join(self.files_path, name)
        if not os.path.exists(file_path):
            raise Exception("not such file in trash")
        info_name = name + ".json"
        info = self._get_trashinfo(info_name)
        return file_path, info

    def get_content(self):
        files = []
        for name in self.get_files():
            try:
                info = self._get_trashinfo(name + ".json")
                files.append((name, info))
            except Exception as e:
                self.logger.info("trash: %s", str(e))
        return files

    def show(self):
        # create_path_if_not_exists(self.files_path, self.info_path)
        self.automatically_clear()
        files = self.get_content()
        files.sort()
        page = Page("Name", "Type", "Path", "Deletion date", "Size")
        for f in files:
            try:
                name = f[0]
                info = f[1]["trash_info"]
                ftype = info["type"]
                path = info["path"]
                deletion_date = info["deletion_date"]
                size = formated_size(info["size"])
                page.add_row(name, ftype, path, deletion_date, size)
            except Exception:
                self.logger.error("show: unknown file \'%s\'", f)
        return page

    def clear(self, name):
        self.logger.debug("attempt to clear \'%s\'", name)
        create_path_if_not_exists(self.files_path, self.info_path)
        file_path, info = self._get_file_and_info(name)
        info_path = os.path.join(self.info_path, name + ".json")
        trash_file = F(file_path)
        info_file = F(info_path)
        status = self.run(trash_file.remove)()
        self.run(info_file.remove)()
        # self.run(self._trashed_files.delete)(file_path)
        self.logger.info("cleared \'%s\'", name)
        return status

    def automatically_clear(self):
        policies = self.clear_policies
        self.logger.info("automatic cleaning is started")
        create_path_if_not_exists(self.files_path, self.info_path)
        content = self.get_content()
        cleanable_trash = set()
        for policy in policies:
            cleanable_trash |= policy.get_trash(content)
        try:
            for trash_file in cleanable_trash:
                self.clear(trash_file)
        except Exception as e:
            self.logger.error("clear: file \'%s\' was not cleared\nerror: %s",
                              trash_file, str(e))
        self.logger.info("automatic cleaning is finished")


# class TrashContainer(list):

#     def add(self, obj):
#         self.append(obj)

#     def delete(self, path):
#         for file in self:
#             if file.trash_path == path:
#                 self.remove(file)


# class TrashedFile(object):

#     def __init__(trash_path, original_path, deletion_date, size, type):
#         self.trash_path = trash_path
#         self.original_path = original_path
#         self.deletion_date = deletion_date
#         self.size = size
#         self.type = type
