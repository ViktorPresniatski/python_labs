import datetime
from django.utils import timezone

from utilite_trash.config.configs import Config


class Setting(object):

    def __init__(self, params={}):
        self.config = Config()
        configs = []
        configs.append(self.config.get_default_config())
        configs.append(self.config.get_config_from_file())
        if params.get("config_file"):
            configs.append(self.config.get_config_from_file(
                           params["config_file"]))
        if params.get("trash_path"):
            configs.append({"trash": {"path": params["trash_path"]}})
        settings = self.config.merge(configs)
        self._trash = TrashSetting(settings["trash"])
        self._remove = RemoveSetting(settings["remove_policy"])
        self._restore = RestoreSetting(settings["restore_policy"])
        self._clear = ClearSetting(settings["clear_policy"])
        if "trash_max_size" in params:
            self._trash._max_size = params["trash_max_size"]
        if params.get("trash_name_duplicate"):
            self._trash._name_duplicate = params["trash_name_duplicate"]
        if "clear_by_size" in params and params["clear_by_size"] is None:
            self._clear.by_size = "off"
        elif params.get("clear_by_size"):
            self._clear.by_size = params["clear_by_size"]
        if "clear_by_date" in params and params["clear_by_date"] is None:
            self._clear.by_date = "off"
        elif params.get("clear_by_date"):
            delta = params["clear_by_date"] / 86400.0
            self._clear.by_date = datetime.timedelta(delta)
        if params.get("remove_policy"):
            self._remove.access_action = params.get("remove_policy")

    @property
    def trash(self):
        return self._trash

    @property
    def remove_policy(self):
        return self._remove

    @property
    def restore_policy(self):
        return self._restore

    @property
    def clear_policy(self):
        return self._clear

class TrashSetting:

    def __init__(self, settings):
        self._path = settings["path"]
        self._max_size = get_size(settings["max_size"])
        self._name_duplicate = settings["name_duplicate"]

    @property
    def path(self):
        return self._path

    @property
    def max_size(self):
        return self._max_size

    @property
    def name_duplicate(self):
        name_duplicate = ["get_uniq", "replace"]
        if self._name_duplicate not in name_duplicate:
            raise Exception("Unknown policy name_duplicate")
        return self._name_duplicate

class RemoveSetting:

    def __init__(self, settings):
        self._access_action = settings["access_action"]

    @property
    def access_action(self):
        access_action = ["default", "ask", "force"]
        if self._access_action not in access_action:
            raise Exception("Unrknown policy access_action")
        return self._access_action

    @access_action.setter
    def access_action(self, value):
        correct_values = ["force, ask, default"]
        if value not in correct_values:
            raise Exception("Incorrect access_action value")
        self._access_action = value

class RestoreSetting:

    def __init__(self, settings):
        self._folder_not_found = settings["folder_not_found"]
        self._name_duplicate = settings["name_duplicate"]

    @property
    def folder_not_found(self):
        folder_not_found = ["create", "ask"]
        if self._folder_not_found not in folder_not_found:
            raise Exception("Unrknown policy folder_not_found")
        return self._folder_not_found

    @property
    def name_duplicate(self):
        name_duplicate = ["get_uniq", "replace"]
        if self._name_duplicate not in name_duplicate:
            raise Exception("Unknown policy name_duplicate")
        return self._name_duplicate

class ClearSetting:

    def __init__(self, settings):
        self._by_date = get_timedelta(settings["by_date"])
        self._by_size = get_size(settings["by_size"])

    @property
    def by_date(self):
        return self._by_date

    @property
    def by_size(self):
        return self._by_size


def get_size(size_string):
    if size_string == "off":
        return "off"
    try:
        size, exp = size_string.split('.')
    except Exception:
        raise Exception("config: incorrect config file")
    size = int(size)
    if exp == "kb":
        size *= 1024
    elif exp == "mb":
        size *= 1024 ** 2
    elif exp == "b":
        size
    else:
        raise Exception("config: incorrect dimention size")
    return size


def get_timedelta(timedelta):
    if timedelta == "off":
        return "off"
    try:
        delta, exp = timedelta.split('.')
    except Exception:
        raise Exception("config: incorrect config file")
    delta = int(delta)
    if exp == "y":
        delta = datetime.timedelta(days=365 * delta)
    elif exp == "m":
        delta = datetime.timedelta(days=30 * delta)
    elif exp == "d":
        delta = datetime.timedelta(days=delta)
    elif exp == "H":
        delta = datetime.timedelta(hours=delta)
    elif exp == "M":
        delta = datetime.timedelta(minutes=delta)
    elif exp == "S":
        delta = datetime.timedelta(seconds=delta)
    else:
        raise Exception("config: incorrect dimention timedelta")
    return delta
