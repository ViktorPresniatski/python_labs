import logging
import pydoc

from utilite_trash.actions.settings import Setting
from utilite_trash.models.file_model import F
import utilite_trash.models.policy as policy
from utilite_trash.models.trash import Trash
from utilite_trash.helpers.helper import formated_string


class Action(object):

    def __init__(self, **kwargs):
        self.logger = self.initialize_logger()
        self.settings = self.get_settings(kwargs)
        trash = self.settings.trash
        clear_policies = self._select_clear_policies()
        self.trash = Trash(trash.path, trash.max_size,
                           trash.name_duplicate, clear_policies)

    def initialize_logger(self):
        logger = logging.getLogger('action')
        logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.WARNING)
        formatter = logging.Formatter('%(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        return logger

    def verbosity(self):
        self.logger.setLevel(logging.INFO)

    def silence(self):
        self.settings.remove_policy.access_action = "force"
        self.logger.setLevel(logging.FATAL)

    def dry_run(self):
        self.trash.dried = True
        self.verbosity()

    def get_settings(self, params):
        # try:
        return Setting(params)
        # except Exception as e:
            # self.logger.error("%s", str(e))
            # exit(e)

    def run(self, **args):
        if args.get("dry_run"):
            self.dry_run()
        if args.get("verbose"):
            self.verbosity()
        elif args.get("silent"):
            self.silence()
        self.trash.automatically_clear()
        if args.get("operation") == "remove":
            files = args["file"]
            if args.get("force"):
                self.settings.remove_policy.access_action = "force"
            if args.get("interactive"):
                self.settings.remove_policy.access_action = "ask"
            remove_method = "remove_file"
            if args.get("recursive"):
                remove_method = "remove_recursive"
            elif args.get("dir"):
                remove_method = "remove_empty_dir"
            pattern = args.get("regex")
            self.remove(files, remove_method, pattern)
        elif args.get("operation") == "restore":
            self.restore(args.get("file", []), args.get("all"))
        elif args.get("operation") == "show":
            self.show()
        elif args.get("operation") == "clear":
            self.clear(args.get("file", []), args.get("all"))

    def remove(self, files, remove_method="remove_recursive",
               pattern=None, one=False):
        statuses = []
        policy = self._select_remove_policy()
        file_list = []
        if pattern:
            file_list = self._get_file_list_by_pattern(pattern, files)
            remove_method = "remove_recursive"
        else:
            file_list = files

        for path in file_list:
            try:
                file = F(path)
                status = self.trash.add(file, policy, remove_method)
            except Exception as e:
                status = False
                self.logger.error("rm: cannot remove \'%s\': %s", path, str(e))
            statuses.append({'path': path, 'status': status})
        return statuses[0]['status'] if one else statuses

    def restore(self, names=[], all=False):
        restore_policy = self._select_restore_policy()
        name_collision_policy = self._select_name_collision_policy()
        if all:
            names = self.trash.get_files()
        for name in names:
            try:
                self.trash.restore(name, restore_policy,
                                   name_collision_policy)
            except Exception as e:
                self.logger.error("restore: cannot restore \'%s\': %s",
                                  name, str(e))

    def show(self):
        page = self.trash.show()
        content = ""
        content = formated_string(page.col_name, page.col_type, page.col_path,
                                  page.col_deletion_date, page.col_size)
        content += "\n"
        for item in page.content:
            try:
                line = formated_string(item.name, item.type, item.path,
                                       item.deletion_date, item.size)
                content += line
            except Exception:
                self.logger.error("show: unknown file \'%s\'", )
        pydoc.pager(content)

    def clear(self, names=[], all=False):
        if all:
            names = self.trash.get_files()
        for name in names:
            try:
                self.trash.clear(name)
            except Exception as e:
                self.logger.error("clear: cannot clear \'%s\': %s",
                                  name, str(e))

    def _select_remove_policy(self):
        if self.settings.remove_policy.access_action == "force":
            return policy.ForcePolicy()
        elif self.settings.remove_policy.access_action == "ask":
            return policy.AskPolicy()
        elif self.settings.remove_policy.access_action == "default":
            return policy.DefaultPolicy()

    def _get_file_list_by_pattern(self, pattern, files):
        file_list = []
        for path in files:
            list = self.trash.grep(path, pattern)
            file_list.extend(list)
        return reversed(file_list)

    def _select_restore_policy(self):
        if self.settings.restore_policy.folder_not_found == "create":
            return policy.DefaultPolicy()
        elif self.settings.restore_policy.folder_not_found == "ask":
            return policy.AskPolicy()

    def _select_name_collision_policy(self):
        if self.settings.restore_policy.name_duplicate == "get_uniq":
            return policy.DefaultPolicy()
        elif self.settings.restore_policy.name_duplicate == "replace":
            return policy.AskPolicy()

    def _select_clear_policies(self):
        policies = []
        value_date = self.settings.clear_policy.by_date
        value_size = self.settings.clear_policy.by_size
        if not value_date == "off":
            policies.append(policy.ClearByDatePolicy(value_date))
        if not value_size == "off":
            policies.append(policy.ClearBySizePolicy(value_size))
        return policies
