#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from utilite_trash.actions.actions import Action
import utilite_trash.arg_parser as arg_parser


def main():
    args = arg_parser.parse_args()
    params = {}
    params["config_file"] = args.config_file
    params["trash_path"] = args.trash
    action = Action(**params)
    action.run(**vars(args))


if __name__ == "__main__":
    main()
