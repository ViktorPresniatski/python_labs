import json
import os
from ConfigParser import ConfigParser


class Config:

    CONFIG_PATH = "~/.trashconfig"

    def __init__(self):
        self.config = ConfigParser()

    def get_default_config(self):
        dirname = os.path.dirname(os.path.realpath(__file__))
        config_path = os.path.join(dirname, "default_config.json")
        hash = self.read_json(config_path)
        del_key = []
        for key, value in hash.iteritems():
            if type(value) is not dict:
                del_key.append(key)
        for key in del_key:
            del hash[key]
        return hash

    def get_config_from_file(self, config_path=CONFIG_PATH):
        config_path = os.path.expanduser(config_path)
        config = {}
        try:
            config = self.read_config(config_path)
        except Exception:
            raise Exception("trash: incorrect user config file")
        return config

    def read_config(self, config_path):
        config = {}
        self.config.read(config_path)

        for section in self.config.sections():
            config[section] = {}
            for option in self.config.options(section):
                config[section][option] = self.config.get(section, option)
        return config

    def read_json(self, config_path):
        config = {}
        with open(config_path) as file:
            try:
                config = json.load(file)
            except Exception:
                raise Exception("trash: incorrect config json file")
        return config

    def merge(self, configs_array):
        result = {}
        for config in configs_array:
            for section, options in config.iteritems():
                if section not in result:
                    result[section] = {}
                hash = {k: v for k, v in options.iteritems() if v is not None}
                result[section].update(hash)
        return result
