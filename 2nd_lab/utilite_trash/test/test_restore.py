import os
import shutil
import unittest

from utilite_trash.models.file_model import F
from utilite_trash.models.policy import DefaultPolicy, ForcePolicy
from utilite_trash.models.trash import Trash


class TestRestoreMethods(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.trash_path = os.path.join(self.dirpath, "Trash")
        self.trash = Trash(self.trash_path, 1000, "get_uniq")
        if not os.path.exists("root/dir1"):
            os.makedirs("root/dir1")
        if not os.path.exists("root/dir2"):
            os.makedirs("root/dir2")
        with open("root/f1", 'w') as f:
            f.write("qwertyui")
        with open("root/dir1/file1", 'w') as f:
            f.write("qwer")
        with open("root/dir1/file2", 'w') as f:
            f.write("q")
        self.root = F("root")
        self.dir1 = F("root/dir1")
        self.dir2 = F("root/dir2")
        self.file = F("root/f1")
        self.file1 = F("root/dir1/file1")
        self.file2 = F("root/dir1/file2")
        self.policy = DefaultPolicy()
        self.trash.add("remove", self.file1, ForcePolicy())
        self.trash.add("remove", self.dir2, ForcePolicy())

    def tearDown(self):
        if os.path.exists("root"):
            shutil.rmtree("root")
        if os.path.exists(self.trash_path):
            shutil.rmtree(self.trash_path)

    def test_restore(self):

        file_path = os.path.join(self.trash_path, "files", self.file1.name)
        name = self.file1.name
        info_name = name + ".json"
        info_path = os.path.join(self.trash_path, "info", info_name)
        self.trash.restore(name, self.policy, self.policy)
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.file1.original_path))

        file_path = os.path.join(self.trash_path, "files", self.dir2.name)
        name = self.dir2.name
        info_name = name + ".json"
        info_path = os.path.join(self.trash_path, "info", info_name)
        self.trash.restore(name, self.policy, self.policy)
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.dir2.original_path))

    def test_dry_run_restore(self):
        file_path = os.path.join(self.trash_path, "files", self.file1.name)
        name = self.file1.name
        info_name = name + ".json"
        info_path = os.path.join(self.trash_path, "info", info_name)
        self.trash.dried = True
        self.trash.restore(name, self.policy, self.policy)
        self.assertTrue(os.path.exists(file_path))
        self.assertTrue(os.path.exists(info_path))
        self.assertFalse(os.path.exists(self.file1.original_path))

    def test_restore_with_uniq_name(self):
        open("root/dir1/file1", 'w').close()
        file = F("root/dir1/file1")
        self.trash.add("remove", file, ForcePolicy())
        name = file.name
        self.trash.restore(name, self.policy, self.policy)
        self.trash.restore(name + "_2", self.policy, self.policy)
        self.assertTrue(os.path.exists(file.original_path))
        self.assertTrue(os.path.exists(file.original_path + "_2"))

    def test_restore_in_non_exist_folder(self):
        shutil.rmtree(self.dir1.original_path)
        self.assertFalse(os.path.exists(self.dir1.original_path))
        file_path = os.path.join(self.trash_path, "files", self.file1.name)
        name = self.file1.name
        info_name = name + ".json"
        info_path = os.path.join(self.trash_path, "info", info_name)
        self.trash.restore(name, self.policy, self.policy)
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.file1.original_path))


if __name__ == "__main__":
    unittest.main()
