import os
import shutil
import unittest

from utilite_trash.models.file_model import F
from utilite_trash.models.policy import ForcePolicy
from utilite_trash.models.trash import Trash


class TestRemoveMethods(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.trash_path = os.path.join(self.dirpath, "Trash")
        self.trash = Trash(self.trash_path, 10, "get_uniq")
        if not os.path.exists("root/dir1"):
            os.makedirs("root/dir1")
        if not os.path.exists("root/dir2"):
            os.makedirs("root/dir2")
        with open("root/f1", 'w') as f:
            f.write("qwertyui")
        with open("root/dir1/file1", 'w') as f:
            f.write("qwer")
        with open("root/dir1/file2", 'w') as f:
            f.write("q")
        self.root = F("root")
        self.dir1 = F("root/dir1")
        self.dir2 = F("root/dir2")
        self.file = F("root/f1")
        self.file2 = F("root/dir1/file1")
        self.file3 = F("root/dir1/file2")
        self.policy = ForcePolicy()

    def tearDown(self):
        if os.path.exists("root"):
            shutil.rmtree("root")
        if os.path.exists(self.trash_path):
            shutil.rmtree(self.trash_path)

    def test_remove_dir(self):
        with self.assertRaises(Exception):
            self.trash.remove_empty_dir(self.file, self.policy)
        with self.assertRaises(Exception):
            self.trash.remove_empty_dir(self.file2, self.policy)
        with self.assertRaises(Exception):
            self.trash.remove_empty_dir(self.file3, self.policy)

    def test_remove_file(self):
        with self.assertRaises(Exception):
            self.trash.remove_file(self.root, self.policy)
        with self.assertRaises(Exception):
            self.trash.remove_file(self.dir1, self.policy)
        with self.assertRaises(Exception):
            self.trash.remove_file(self.dir2, self.policy)

    def test_remove(self):
        name = os.path.basename(self.dir2.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove(self.dir2, self.policy), trash_path)

        name = os.path.basename(self.file.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove(self.file, self.policy), trash_path)

        name = os.path.basename(self.file2.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove(self.file2, self.policy), trash_path)

        name = os.path.basename(self.file3.original_path)
        os.chmod(self.dir1.original_path, 0o000)
        self.assertIs(self.trash.remove(self.file3, self.policy), None)
        os.chmod(self.dir1.original_path, 0o777)

    def test_remove_recursive(self):
        name = os.path.basename(self.root.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove_recursive(self.root, self.policy), trash_path)
        self.assertFalse(os.path.exists(self.root.original_path))
        self.assertFalse(os.path.exists(self.dir1.original_path))
        self.assertFalse(os.path.exists(self.dir2.original_path))
        self.assertFalse(os.path.exists(self.file.original_path))
        self.assertFalse(os.path.exists(self.file3.original_path))
        self.assertFalse(os.path.exists(self.file2.original_path))

        os.makedirs("root/dir1")
        os.makedirs("root/dir2")
        open("root/f1", 'w').close()
        open("root/dir1/file1", 'w').close()
        open("root/dir1/file2", 'w').close()
        os.chmod(self.root.original_path, 0o000)
        self.assertRaises(self.trash.remove_recursive(self.root, self.policy), None)
        os.chmod(self.root.original_path, 0o777)

        os.chmod(self.dir1.original_path, 0o000)
        self.trash.remove_recursive(self.root, self.policy)
        self.assertFalse(os.path.exists(self.dir2.original_path))
        self.assertTrue(os.path.exists(self.dir1.original_path))
        os.chmod(self.dir1.original_path, 0o777)

    def test_dry_run_removing(self):
        self.trash.dried = True
        name = os.path.basename(self.file.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove(self.file, self.policy), trash_path)
        self.assertTrue(os.path.exists(self.file.original_path))

        name = os.path.basename(self.file2.original_path)
        trash_path = os.path.join(self.trash_path, "files/{}".format(name))
        self.assertEquals(self.trash.remove(self.file2, self.policy), trash_path)
        self.assertTrue(os.path.exists(self.file2.original_path))

if __name__ == "__main__":
    unittest.main()
