import os
import shutil
import unittest

from utilite_trash.models.file_model import Directory
from utilite_trash.models.file_model import F
from utilite_trash.models.file_model import File
from utilite_trash.models.file_model import Link


class TestFileModelMethods(unittest.TestCase):

    def setUp(self):
        os.makedirs("root/dir1")
        os.makedirs("root/dir2")
        with open("root/f1", 'w') as f:
            f.write("qwertyui")
        with open("root/dir1/file1", 'w') as f:
            f.write("qwer")
        with open("root/dir1/file2", 'w') as f:
            f.write("q")
        os.symlink(os.path.abspath("root/dir1/file2"),
                   os.path.abspath("root/dir1/link"))
        self.root = "root"
        self.file = "root/f1"
        self.link = "root/dir1/link"

    def tearDown(self):
        shutil.rmtree("root")

    def test_f(self):
        self.assertIsInstance(F(self.file), File)
        self.assertIsInstance(F(self.root), Directory)
        self.assertIsInstance(F(self.link), Link)

    def test_listdir(self):
        result = [F("root/dir1").original_path, F("root/dir2").original_path,
                  F("root/f1").original_path]
        array = []
        for f in F(self.root).listdir():
            array.append(f.original_path)
        self.assertEquals(array, result)

    def test_remove_empty_dir(self):
        self.assertRaises(Exception, F(self.root).move)


if __name__ == "__main__":
    unittest.main()
