import datetime
import os
import shutil
import time
import unittest

from utilite_trash.models.file_model import F
from utilite_trash.models.policy import ClearByDatePolicy
from utilite_trash.models.policy import ClearBySizePolicy
from utilite_trash.models.policy import DefaultPolicy
from utilite_trash.models.policy import ForcePolicy
from utilite_trash.models.trash import Trash


class TestClearMethods(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.trash_path = os.path.join(self.dirpath, "Trash")
        self.trash = Trash(self.trash_path, 1000, "get_uniq")
        if not os.path.exists("root/dir1"):
            os.makedirs("root/dir1")
        if not os.path.exists("root/dir2"):
            os.makedirs("root/dir2")
        with open("root/f1", 'w') as f:
            f.write("qwertyui")
        with open("root/dir1/file1", 'w') as f:
            f.write("qwer")
        with open("root/dir1/file2", 'w') as f:
            f.write("q")
        self.root = F("root")
        self.dir1 = F("root/dir1")
        self.dir2 = F("root/dir2")
        self.file = F("root/f1")
        self.file1 = F("root/dir1/file1")
        self.file2 = F("root/dir1/file2")
        self.policy = DefaultPolicy()
        self.trash_file = F(self.trash.add("remove", self.file, ForcePolicy()))
        self.trash_file1 = F(self.trash.add("remove", self.file1, ForcePolicy()))
        self.trash_file2 = F(self.trash.add("remove", self.file2, ForcePolicy()))
        self.trash_dir2 = F(self.trash.add("remove", self.dir2, ForcePolicy()))

    def tearDown(self):
        if os.path.exists("root"):
            shutil.rmtree("root")
        if os.path.exists(self.trash_path):
            shutil.rmtree(self.trash_path)

    def test_clear(self):
        name1 = self.trash_file1.name
        name2 = self.trash_file2.name
        name3 = self.trash_dir2.name
        name4 = "azaza"
        info1 = os.path.join(self.trash_path, "info", name1 + ".json")
        info2 = os.path.join(self.trash_path, "info", name2 + ".json")
        info3 = os.path.join(self.trash_path, "info", name3 + ".json")

        self.assertTrue(os.path.exists(self.trash_file1.original_path))
        self.assertTrue(os.path.exists(info1))
        self.assertTrue(os.path.exists(self.trash_file2.original_path))
        self.assertTrue(os.path.exists(info2))
        self.assertTrue(os.path.exists(self.trash_dir2.original_path))
        self.assertTrue(os.path.exists(info3))

        self.trash.clear(name1)
        self.trash.clear(name2)
        self.trash.clear(name3)
        with self.assertRaises(Exception):
            self.trash.clear(name4)

        self.assertFalse(os.path.exists(self.trash_file1.original_path))
        self.assertFalse(os.path.exists(info1))
        self.assertFalse(os.path.exists(self.trash_file2.original_path))
        self.assertFalse(os.path.exists(info2))
        self.assertFalse(os.path.exists(self.trash_dir2.original_path))
        self.assertFalse(os.path.exists(info3))

    def test_dry_run_clear(self):
        name2 = self.trash_file2.name
        name3 = self.trash_dir2.name
        info2 = os.path.join(self.trash_path, "info", name2 + ".json")
        info3 = os.path.join(self.trash_path, "info", name3 + ".json")
        self.trash.dried = True
        self.assertTrue(os.path.exists(self.trash_file2.original_path))
        self.assertTrue(os.path.exists(info2))
        self.assertTrue(os.path.exists(self.trash_dir2.original_path))
        self.assertTrue(os.path.exists(info3))
        self.trash.clear(name2)
        self.trash.clear(name3)
        self.assertTrue(os.path.exists(self.trash_file2.original_path))
        self.assertTrue(os.path.exists(info2))
        self.assertTrue(os.path.exists(self.trash_dir2.original_path))
        self.assertTrue(os.path.exists(info3))

    def test_automatically_clear_by_date(self):
        policies = [ClearByDatePolicy(datetime.timedelta(seconds=1))]
        time.sleep(1)
        self.trash.automatically_clear(policies)
        name1 = self.trash_file1.name
        name2 = self.trash_file2.name
        name3 = self.trash_dir2.name
        info1 = os.path.join(self.trash_path, "info", name1 + ".json")
        info2 = os.path.join(self.trash_path, "info", name2 + ".json")
        info3 = os.path.join(self.trash_path, "info", name3 + ".json")
        self.assertFalse(os.path.exists(self.trash_file1.original_path))
        self.assertFalse(os.path.exists(info1))
        self.assertFalse(os.path.exists(self.trash_file2.original_path))
        self.assertFalse(os.path.exists(info2))
        self.assertFalse(os.path.exists(self.trash_dir2.original_path))
        self.assertFalse(os.path.exists(info3))

    def test_automatically_clear_by_size(self):
        policies = [ClearBySizePolicy(2)]
        self.trash.automatically_clear(policies)
        name1 = self.trash_file1.name
        name2 = self.trash_file2.name
        name3 = self.trash_file.name
        info1 = os.path.join(self.trash_path, "info", name1 + ".json")
        info2 = os.path.join(self.trash_path, "info", name2 + ".json")
        info3 = os.path.join(self.trash_path, "info", name3 + ".json")
        self.assertFalse(os.path.exists(self.trash_file1.original_path))
        self.assertFalse(os.path.exists(info1))
        self.assertTrue(os.path.exists(self.trash_file2.original_path))
        self.assertTrue(os.path.exists(info2))
        self.assertFalse(os.path.exists(self.trash_file.original_path))
        self.assertFalse(os.path.exists(info3))


if __name__ == "__main__":
    unittest.main()
