import os
import shutil
import unittest

from utilite_trash.models.file_model import F
from utilite_trash.models.policy import ForcePolicy
from utilite_trash.models.trash import Trash


class TestTrashMethods(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.trash_path = os.path.join(self.dirpath, "Trash")
        self.trash = Trash(self.trash_path, 1000, "get_uniq")
        if not os.path.exists("root/dir1"):
            os.makedirs("root/dir1")
        if not os.path.exists("root/dir2"):
            os.makedirs("root/dir2")
        with open("root/f1", 'w') as f:
            f.write("qwertyui")
        with open("root/dir1/file1", 'w') as f:
            f.write("qwer")
        with open("root/dir1/file2", 'w') as f:
            f.write("q")
        self.root = F("root")
        self.dir1 = F("root/dir1")
        self.dir2 = F("root/dir2")
        self.file = F("root/f1")
        self.file1 = F("root/dir1/file1")
        self.file2 = F("root/dir1/file2")

    def tearDown(self):
        if os.path.exists(self.trash_path):
            shutil.rmtree(self.trash_path)
        if os.path.exists("root"):
            shutil.rmtree("root")

    def test_grep(self):
        files = []
        files.append(self.file.original_path)
        files.append(self.file2.original_path)
        files.append(self.file1.original_path)
        results = self.trash.grep(self.root.original_path, "f.*")
        self.assertEquals(files, results)
        files = []
        files.append(self.dir1.original_path)
        files.append(self.dir2.original_path)
        results = self.trash.grep(self.root.original_path, "d.*")
        self.assertEquals(files, results)

    def test_get_trash_info(self):
        name = self.file.name
        self.trash.add("remove", self.file, ForcePolicy())
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "info",
                                                    name + ".json")))
        info = self.trash._get_trashinfo(name + ".json")
        self.assertIsInstance(info, dict)
        with self.assertRaises(Exception):
            self.trash._get_trashinfo("sdf")

    def test_get_file(self):
        self.trash.add("remove", self.file, ForcePolicy())
        self.trash.add("remove", self.dir2, ForcePolicy())
        self.trash.add("remove", self.file1, ForcePolicy())
        files = ["file1", "dir2", "f1"]
        result = self.trash.get_files()
        self.assertEquals(files, result)

    def test_get_file_and_info(self):
        trash_path = self.trash.add("remove", self.file, ForcePolicy())
        path, info = self.trash._get_file_and_info(self.file.name)
        self.assertIsInstance(info, dict)
        self.assertEquals(trash_path, path)


if __name__ == "__main__":
    unittest.main()
