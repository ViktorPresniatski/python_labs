from setuptools import find_packages, setup

setup(
    name='trash',
    version=1.0,
    packages=find_packages(),
    package_data={"utilite_trash.config": ["default_config.json"]},
    long_description='Utilite of working with trash',
    entry_points={
        'console_scripts': ['trash = utilite_trash.main:main']
    }
)
